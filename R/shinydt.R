#' selectableDataTable
#' 
#' Creates the selectable data table
#'
#' @param id The id used to identify the data table (input$id, output$id)
#'
#' @export
#'
#' @examples    # Creation of selectableDataTable object (table output)
#'
#' # ui.R
#' selectableDataTable('table.id')
#'
#' # server.R
#' output$table.id <- renderSelectableDataTable({
#'   table.data
#'
#' # selectableDataTable also contains the selection as input data
#' # selection indices start from 0 so the first row = 0, second row = 1, etc.
#' input$table.id
#' })
selectableDataTable <- function(id) {
  tagList(
    tags$head(
      singleton(tags$script(src='shinydt/js/underscore-min.js', type = 'text/javascript')),
      singleton(tags$script(src = 'shinydt/js/jquery.dataTables.min.js', type = 'text/javascript')),
      singleton(tags$script(src = 'shinydt/js/dataTables.bootstrap.js', type = 'text/javascript')),
      singleton(tags$script(src='shinydt/js/dataTables.tableTools.min.js', type = 'text/javascript')),
      singleton(tags$script(src='shinydt/js/selectable_datatable.js', type = 'text/javascript')),
      singleton(tags$script(src='shinydt/js/selectable_datatable_shiny_bindings.js', type = 'text/javascript')),
      singleton(tags$link(href = 'shinydt/css/DT_bootstrap.css', rel = "stylesheet", type = "text/css")),
      singleton(tags$link(href = 'shinydt/css/dataTables.tableTools.css', rel = "stylesheet", type = "text/css"))
    ),
    div(id=id, class='selectable_datatable')
  )
}

#' renderSelectableDataTable
#'
#' Renders the data table using the DataTables JavaScript library
#'
#' @param expr The computation that leads to an output
#' @param single Boolean that is used to indicate if the data table will use single selection (TRUE)
#'    or multi selection (FALSE)
#' @export
#' @examples    # pass in a data.frame or a matrix to selectableDatatable
#' renderSelectableDataTable({
#'   table.data
#' })
#'
#' # pass in a list with data to selectableDatatable
#' renderSelectableDataTable({
#'   list(
#'     df = table.data
#'   )
#' })
#'
#' renderSelectableDataTable({
#'   list(
#'     df = table.data,
#'     cols = column.names
#'   )
#' })
#'
#' # pass in a option to set the table to single selection instead of multi selection
#' renderSelectableDataTable({
#'   table.data
#' }, single=TRUE)
renderSelectableDataTable <- function(expr, env = parent.frame(), quoted = FALSE,
  options = NULL, single = FALSE) {
  func <- shiny::exprToFunction(expr, env, quoted)

  function() {
    data <- func()

    df <- NULL
    cols <- NULL
    if(is.list(data)) {
      df <- data$df
      if(is.null(df)) {
        if(is.data.frame(data) || is.matrix(data)) {
          if(!is_valid_input(data)) return()
          df <- data
          # Replaces '.' characters withing the column names with whitespace in order to allow the data table
          # to read the column names correctly
          colnames(df) <- sapply(colnames(df), function(x){gsub('.', ' ', x, fixed=TRUE)}, USE.NAMES=FALSE)
          cols <- colnames(df)
        } else {
          warning('df is NULL')
          return()
        }
      }
      cols <- data$cols
      if(is.null(cols)) {
        colnames(df) <- sapply(colnames(df), function(x){gsub('.', ' ', x, fixed=TRUE)}, USE.NAMES=FALSE)
        cols <- colnames(df)
      } else {
        cols <- sapply(cols, function(x){gsub('.', ' ', x, fixed=TRUE)}, USE.NAMES=FALSE)
        df <- df[,data$cols]
      }
    } 

    colnames(df) <- cols

    list(
      dt_row_array = as.matrix(df),
      cols = cols,
      single_select = single
    )
  }
}
