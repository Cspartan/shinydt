/**
 * SelectableDataTable extends the functionality of DataTables and DataTables 
 * TableTools to allow the user to select from a DataTable with the ability to do 
 * de/select all of filtered rows. 
 */
export class SelectableDataTable

  /**
   * Array of the selected row indices
   * @type {Integer Array}
   */
  selected_row_indices: []


  /**
   * Class that creates a data table using the dataTables.js library
   * @param {Element} el Element that is creating the data table
   * @param {Object} table_data Data that is either an HTML table (is_html == true) or 2D array of values 
   *         for the creation of the HTML table (may be either an Array(row data) or a String(HTML))
   * @param {Array} cols Column names used for the creation of the HTML table
   * @param {Boolean} single_select Determines whether the has multi or single selection enabled
   * @constructor
   */
  (@_el, table_data, cols, single_select) ->

    _this = this
    el_id = @_el.id

    /**
     * Array of formated column data for use in the data table's settings
     * @type {Array}
     */
    col_data = @_format_col_data(cols)

    @$el = $ @_el 

    lazy_handle_row_selection = _.debounce (!~> @_handle_row_selection!), 250

    if single_select
      @selection = \single
    else
      @selection = \multi

    /**
     * Settings that will be used by the tableTools extention for dataTables
     * @type {Object}
     */
    table_tools_settings = 
      sRowSelect: @selection
      /**
       * Handles the row selection for the data table
       * @param {Node} Row that was selected
       */
      fnRowSelected: lazy_handle_row_selection
      /**
       * Handles the row deselection for the data table
       * @param {Node} Row that was deselected
       */
      fnRowDeselected: lazy_handle_row_selection

    # Buttons
    if !single_select
      aButtons = 
        * sExtends: 'text'
          sButtonText: ' Select all filtered rows'
          fnClick: (btn, cfg, flash) !~>
            tr = @table.$ 'tr', filter:'applied'
            tt = @_tt
            tr.each !-> tt._fnRowSelect this
        * sExtends: 'text'
          sButtonText: ' Deselect all filtered rows'
          fnClick: (btn, cfg, flash) !~>
            tr = @table.$ 'tr', filter:'applied'
            tt = @_tt
            tr.each !-> tt._fnRowDeselect this
        * sExtends: 'text'
          sButtonText: ' Deselect all rows'
          fnClick: (btn, cfg, flash) !~>
            tt = @_tt
            tt.fnSelectNone!
            # tr = @table.$ 'tr'
            # tr.each !-> tt._fnRowDeselect this
        * sExtends: 'text'
          sButtonText: ' Invert selection'
          fnClick: (btn, cfg, flash) !~>
            tr = @table.$ 'tr'
            tr_inverse = _.reject tr, -> it.classList.contains \selected
            tt = @_tt
            @table.$ 'tr.selected' 
              .each !-> tt._fnRowDeselect this
            $ tr_inverse 
              .each !-> tt._fnRowSelect this
        * sExtends: 'text'
          sButtonText: ' Clear column filters'
          fnClick: (btn, cfg, flash) !~>
            @_filter_inputs.val ''
            @_filter_inputs.trigger \keyup
      table_tools_settings.aButtons = aButtons
    else
      table_tools_settings.aButtons = []

    /**
     * Settings that will be used in the creation of the data table
     * @type {Object}
     */
    settings = do
      data: table_data
      columns: col_data
      destroy: true
      lengthMenu: [5, 10, 20, 30, 50]
      pageLength: 10
      orderClasses: false
      order: []
      paging: true
      pagingType: 'full_numbers'
      processing: true
      dom: 'T<"clear">lrtip'
      tableTools: table_tools_settings
      autoWidth: false
      stateSave: true
      stateDuration: -1
      stateSaveParams: (settings, data) !~>
        data.sel_idx = @selected_row_indices
        data.num_rows = settings.aoData.length
        # return unless @_filter_inputs?
        # @input_val := {}
        # for input in @_filter_inputs
        #   $input = $ input
        #   v = $input.prop('value')
        #   k = $input.prop('placeholder')
        #   if v != ''
        #     @input_val[k] = v
        # console.log @input_val
      stateLoadParams: (settings, data) ~>
        if table_data.length != data.num_rows
          # console.log 'number of rows not equal to saved number of rows'
          # console.log \table_data.length , table_data.length
          # console.log \data.num_rows , data.num_rows
          return false
        data.search.search = ''
        for col in data.columns
          col.search.search = ''
        @selected_row_indices = data.sel_idx
        # console.log '@input_val', @input_val
        return true
      initComplete: (settings, json) !->
        tt = TableTools.fnGetInstance(settings.sTableId)
        nodes = @fnGetNodes!
        for idx in _this.selected_row_indices
          tt.fnSelect(nodes[idx])
      
    /**
     * HTML for the footer filter elements
     * @type {String}
     */
    footer_inputs = @_generate_footer(cols)

    /**
     * The table used by dataTables to create the data table
     */
    $("\##{el_id}").html "<table cellpadding='0' cellspacing='0' border='0' class='display table selectable-datatable' id='#{el_id} table'><tfoot>#{footer_inputs}</tfoot></table>"

    /**
     * The data table
     * Initialize DataTable with settings
     * @type {Node}
     */
    table = @$el.find "table" .DataTable settings

    @table := table

    /**
     * Initialize TableTools for row selection handling
     */
    @_tt := @table.tabletools!

    /**
     * The table filter function uses the custom searching of columns
     * based tfoot input search values
     */
    filter_inputs = @$el.find "table tfoot input"
    @_filter_inputs := filter_inputs
    table.columns!.eq 0
      .each (col_idx) !->
        col_filter_func = !-> 
          table.column col_idx
            .search @value
            .draw(false)
        /**
         * Underscore.js debounce method is used to limit execution of the keyup 
         * event to maximum of every 250ms.
         */
        lazy_col_filter_func = _.debounce col_filter_func, 250
        $ \input, table.column(col_idx).footer!
          .on 'keyup change', lazy_col_filter_func

    /**
     * Add Bootstrap pagination class to table pagination div container so that it
     * is styled appropriately
     */
    dataTables_paginate = @$el.find ".dataTables_paginate"
      .addClass \pagination

    /**
     * If multiple selection is enabled for the SelectableDataTable then restyle
     * the selection handling control buttons
     */
    if !single_select
      /**
       *Show number of selected rows
       */
      sel_rows = jQuery '
        <div class="alert alert-info pull-left">
          <h4 class="selected_status pull-left">
            0 items selected
          </h4>
        </div>'
        .prependTo @$el
      /**
       * Update element showing number of selected rows on selection change
       */
      $ @_el .on \change !~>
        selected_length = @selected_row_indices.length
        p = @$el.find("h4.selected_status")
        p.text "#{selected_length} items selected"

      # Buttons
      sel_btns = @$el.find \.DTTT_button
        .addClass "btn"

      select_all_btn = sel_btns
        .find "span:contains('Select all')"
        .parent!

      jQuery '
          <span class="fa-stack fa-lg">
            <i class="fa fa-table fa-stack-2x"></i>
            <i class="fa fa-check fa-stack-2x" style="color:lime; opacity:0.5;"></i>
          </span>'
        .prependTo select_all_btn

      deselect_all_btn = sel_btns
        .find "span:contains('Deselect all filtered rows')"
        .parent!

      jQuery '
        <span class="fa-stack fa-lg">
          <i class="fa fa-table fa-stack-2x"></i>
          <i class="fa fa-ban fa-stack-2x" style="color:red; opacity:0.5;"></i>
        </span>'
        .prependTo deselect_all_btn

      deselect_all_btn = sel_btns
        .find "span:contains('Deselect all rows')"
        .parent!

      jQuery '
        <span class="fa-stack fa-lg">
          <i class="fa fa-table fa-stack-2x"></i>
          <i class="fa fa-ban fa-stack-2x" style="color:red; opacity:0.9;"></i>
        </span>'
        .prependTo deselect_all_btn

      clear_filters_btn = sel_btns
        .find "span:contains('Clear column filters')"
        .parent!
      jQuery '
        <span class="fa-stack fa-lg">
          <i class="fa fa-edit fa-stack-2x"></i>
          <i class="fa fa-ban fa-stack-2x" style="color:red; opacity:0.6;"></i>
        </span>'
        .prependTo clear_filters_btn

      invert_selection_btn = sel_btns
        .find "span:contains('Invert selection')"
        .parent!
      jQuery '
        <span class="fa-stack fa-lg">
          <i class="fa fa-table fa-stack-2x"></i>
          <i class="fa fa-refresh fa-stack-2x" style="color:DodgerBlue; opacity:0.9;"></i>
        </span>'
        .prependTo invert_selection_btn

  # Helper functions
  /**
   * Contains the code for handleing the row selection using the TableTools 
   * extension
   */
  _handle_row_selection: !->
    @selected_row_indices := @_tt.fnGetSelectedIndexes!
    @table.state.save!
    $ @_el .trigger \change

  /**
   * Formats the column names so that they can be used by data tables
   * Also sets the default content for any cells/nodes that are set to null
   * @param {Array} col_names Array of column names for the data table
   * @return {Array} Array containing the column data that can be used by the 'colunms' 
   *                       option in the data tables settings
   */
  _format_col_data: (col_names) ->
    cols = []
    for col in col_names
      head = {'title': col, 'data': col, 'defaultContent': ''}
      cols.push head
    cols

  /**
   * Creates the text inputs that will control the column filters
   * These inputs will be placed in the footer of the data table
   * @param {Array} col_names Array of column names for the data table
   * @return {String} String containing the HTML for the footer of the data table
   */
  _generate_footer: (col_names) ->
    html = ""
    for col in col_names
      html += """
        <th colspan=1>
          <input type='text' placeholder='#{col}' class='search_init'>
        </th>
        """
    return html
